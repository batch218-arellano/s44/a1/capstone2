const mongoose = require("mongoose");
const Course = require('../models/product');
const bcrypt = require ("bcrypt");
const auth = require("../auth.js");
// Create Product 

module.exports.addProduct = (data) => {
		console.log(data.isAdmin)

		if(data.isAdmin) {
			let newProduct = new Product({
				name: data.product.name,
				description: data.product.description,
				price: data.product.price
			});

			return newProduct.save().then((newProduct, error) => {
				if(error){
					return error
				}

				return newProduct 
			})
		};

		let message = Promise.resolve('User must be ADMIN to access this.')

		return message.then((value) => {
			return {value}
		})
	};



// Retrieve all active products

module.exports.getActiveProducts = () => {
	return Product.find({isActive:true}).then(result => {
		return result
	})
}


//retrieve single product

module.exports.retrieveProduct = (data) => {
	                    //inside the parenthesis should be the id
	return Product.findById(data).then(result => {
		return result;
	})
}

// Update Product Info (admin)


module.exports.updateProduct = (producId, newData) => {
	if(newData.isAdmin == true){
		// update code
		return Product.findByIdAndUpdate(productId , 
			{			//req.body  
				name: newData.course.name,
				description: newData.course.description,
				price: newData.course.price
			}
		).then((updatedProduct, error) => {
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
	}
};

// Archive product (Admin)


module.exports.archiveProduct = (productId) => {
	return Product.findByIdAndUpdate(courseId, {
		isActive: false
	})
	.then((archivedProduct, error) => {
		if(error){
			return false
		} 

		return {
			message: "Hello Admin! Product archived successfully!"
		}
	})
};


