const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");

const auth = require ("../auth.js");


router.post("/create", auth.verify,(req,res) => {
	
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

productController.addProduct(data).then(resultFromController => res.send(resultFromController));

});

router.get("/products", (req,res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))
})

router.get("/products/:productId", (req,res) => {
							
	courseController.getCourse(req.params.courseId).then(resultFromController => res.send(resultFromController))
})

router.put("/products/:productId", auth.verify, (req,res) => 
{
	const newData = {
		course: req.body, 	
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.updateProduct(req.params.courseId, newData).then(resultFromController => {
		res.send(resultFromController)
	});
});

router.patch("/products/:productId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params.courseId).then(resultFromController => {
		res.send(resultFromController)
	});
});

module.exports = router;
