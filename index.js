
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// routers
const userRoute = require("./routes/userRoute.js");
const productRoutes = require("./routes/productRoutes.js");

const app = express();
app.use(cors());

app.use(express.json());

app.use(express.urlencoded({extended:true}));


app.use("/users", userRoute);
app.use("/product", productRoutes);

// Connect to our MongoDB Database
mongoose.connect("mongodb+srv://admin:admin@capstone.osui4cj.mongodb.net/e-commerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Prompts a message once connected
mongoose.connection.once('open', () => console.log('Now connected to Ayson-Mongo DB Atlas.'));

// Prompts a message once connected to port 4000
app.listen(process.env.PORT || 8000, () => 
	{console.log(`API is now online on port ${process.env.PORT || 8000 }`)
}); 


