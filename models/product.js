const mongoose = require ("mongoose");
const productRoutes = require("../routes/productRoutes.js");
const productSchema = new mongoose.Schema ({

	name: {

		type: String,
		required: [true, "Produce Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price:{
		type: Number
	},
	isActive: {
		type: Boolean,
		default: true, 
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [

		{
			userId: {
				type: String,
				requred: [true, "userId is required"]
			},
		purchasedOn : {
			type: Date,
			default: new Date()
		}

	}]

})



module.exports = mongoose.model("Product", productSchema);