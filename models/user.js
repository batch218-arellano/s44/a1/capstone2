const mongoose = require ("mongoose");

const userSchema = new mongoose.Schema ({

	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	}, 
	IsAdmin: {
		type: Boolean,
		default: false
	}, 
	orders: [
	{
		products: [

			{
			productId: {
				type: String,
				required: [true, "productId is empty"]
			},
			quantity: {
				type: Number,
				required: [true, "quantity is empty"]
			},
		}],

		totalAmount: {
			type: Number
		},
		purchasedOn : {
			type: Date,
			default: new Date()
		}

	}]
})

module.exports = mongoose.model("User", userSchema);


